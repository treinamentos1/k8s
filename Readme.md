## Bem-vindo

É um prazer poder contribuir com seu sucesso profissional.
Este material aborda conhecimentos estruturados para simplificar seu aprendizado em K8s.

Explore ao máximo e compartilhe o quanto quizer com quem também precisa. Sinto-me orgulhoso de poder ajudar de alguma forma.


![](imagens/escolha.png)