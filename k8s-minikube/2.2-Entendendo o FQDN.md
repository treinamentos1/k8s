## 12-Entendendo o FQDN

Acesse qualquer pod webapp
```shell
kubectl exec -it webapp-XXXXXXXXX -- bash
```

execute para instalar o nslookup
```shell
apt update && apt install -y dnsutils
```

execute para mapear o caminho kubernetes
```shell
nslookup kubernetes
```

Dentro do retorno, encontramos `kubernetes.default.svc.cluster.local`. Este é o FQDN do cluster

vamos entender...
kubernetes    -> Nome do serviço
default       -> Nome do Namespace
svc           -> Serviço (Kind: Service)
cluster.local -> Nome do cluster
