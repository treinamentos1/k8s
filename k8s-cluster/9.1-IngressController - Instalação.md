### Ingress 

Um objeto de API que gerencia o acesso externo aos serviços em um cluster, geralmente na camada 7 de aplicação, ou seja, no HTTP.

O Ingress pode fornecer balanceamento de carga, terminação SSL e hospedagem virtual baseada em nome. 

![](https://d33wubrfki0l68.cloudfront.net/91ace4ec5dd0260386e71960638243cf902f8206/c3c52/docs/images/ingress.svg)

m Ingress pode ser configurado para fornecer URLs acessíveis externamente aos Serviços, tráfego de balanceamento de carga, encerrar SSL/TLS e oferecer hospedagem virtual baseada em nome. Um **IngressController** é responsável por controlar o Ingress, geralmente com um balanceador de carga. 

### IngressController

Serve para controlar o Ingress.

## Implantando o IngressController chamado (Nginx Ingress)

Instalar o Heml

```bash
curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | sudo tee /usr/share/keyrings/helm.gpg > /dev/null
sudo apt-get install apt-transport-https --yes
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
sudo apt-get update
sudo apt-get install helm
```

Criar um Namespace e, adicionar o repositório do Nginx Ingress e instalar
```bash
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
kubectl create ns ingresscontroller
helm install ingresscontroller-nginx ingress-nginx/ingress-nginx -n ingresscontroller
```

Listar tudo na Namespace *ingress-nginx* e coletar a porta redirecionada da 80 no Service **ingress-nginx-controller**.
```bash
kubectl get all -n ingresscontroller
```

Foi criado além dos objetos listados, um **ingressClasses** que pode ser listado com o seguinte comando:
```bash
kubectl get ingressclasses -n ingresscontroller
``` 

31841





### Fonte:
- https://kubernetes.io/docs/concepts/services-networking/ingress/
- https://kubernetes.io/docs/concepts/services-networking/ingress-controllers/
- https://docs.nginx.com/nginx-ingress-controller/installation/installation-with-helm/