Requisitos Mínimos
Hostname | SO     | CPU | RAM
:--     |:--    |:--|:--
master  |Ubuntu | 2 | 2
worker1 |Ubuntu | 2 | 2
worker2 |Ubuntu | 2 | 2

## Desativar Swap
Parar o Swap do sistema:
```bash
sudo swapoff -a
```

Vá até o arquivo abaixo e desative qualquer menção a Swap
`vim /etc/fstab`
```config
#/swap.img      none    swap    sw      0       0
```

## Container Runtimes

Encaminhando o IPv4 e permitindo que o iptables veja o tráfego em ponte:
```bash
sudo cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
overlay
br_netfilter
EOF

sudo modprobe overlay
sudo modprobe br_netfilter

sudo cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-iptables  = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward                 = 1
EOF

sudo sysctl --system
```

## Containerd - Instalando o Docker.
![](https://assets.mkdev.me/posts/covers/000/000/213/original/the-tool-that-realy-runs-your-containers-deep-dive-into-runc-and-oci-specifications.png?1588013746)

O principal a ser instalado neste momento é apenas o **containerd.io**, mas vamos seguindo com a instalação de todos os componentes da documentação do Docker.

```bash
sudo apt-get update && \
sudo apt-get install ca-certificates curl gnupg && \
sudo install -m 0755 -d /etc/apt/keyrings && \
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg && \
sudo chmod a+r /etc/apt/keyrings/docker.gpg && \
echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null && \
sudo apt-get update && \
sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin && \
sudo usermod -aG docker $USER && \
sudo init 6
```

## cni-plugins - Instalando um Pod de complemento de rede

Você deve implantar um Interface de rede do contêiner Complemento de rede de pods baseado em CNI para que seus pods possam se comunicar uns com os outros. O Cluster DNS (CoreDNS) não será inicializado antes da instalação de uma rede. 

Procure na documentação por **Container Runtimes** e vá até a página do GitHub ne etapa 3, ou siga estas instruções abaixo.
Troque a versão v1.3.0 pela versão mais recente ou da sua escolha, caso precisar.
```shell
wget https://github.com/containernetworking/plugins/releases/download/v1.3.0/cni-plugins-linux-amd64-v1.3.0.tgz && \
sudo mkdir -p /opt/cni/bin && \
sudo tar Cxzvf /opt/cni/bin cni-plugins-linux-amd64-v1.3.0.tgz && \
rm -rf cni-plugins-linux-amd64-v1.3.0.tgz
```

## Configurar o Containerd no arquivo config.toml
Para usar o driver do container runtime, adicione esta linha no final do arquivo:

`sudo vim /etc/containerd/config.toml`
```bash
[plugins."io.containerd.grpc.v1.cri".containerd.runtimes.runc.options]
    SystemdCgroup = true
```

Encontre e desabilite(comente) a linha abaixo:
```config
#disabled_plugins = ["cri"]
```

Reinicie o containerd
```bash
sudo systemctl restart containerd
```

## Instalando o kubeadm, kubelet e kubectl

- kubeadm: o comando para criar o cluster.
- kubelet: o componente que executa em todas as máquinas no seu cluster e cuida de tarefas como a inicialização de pods e contêineres.
- kubectl: a ferramenta de linha de comando para interação com o cluster.

Atualize o índice de pacotes apt e instale os pacotes necessários para utilizar o repositório apt do Kubernetes:
```bash
sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl
```

Faça o download da chave de assinatura pública da Google Cloud:
```bash
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
```
Adicione o repositório apt do Kubernetes:
```bash
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
```

Atualize o índice de pacotes apt, instale o kubelet, o kubeadm e o kubectl, e fixe suas versões. Caso desejar, você pode ver a versão específica para ser instalada:
```bash
apt list -a kubeadm

sudo apt-get install -y kubelet kubeadm kubectl
sudo apt-mark hold kubelet kubeadm kubectl
```

## Criando um cluster com kubeadm

Para criar o cluster, utilizamos o Kubeadm.

Opcionalmente você pode listar as imagens ou ir direto para o pull baixando as imagens antecipadamente.
```bash
sudo kubeadm config images list
sudo kubeadm config images pull
```

Inicie o cluster, escolha uma das opções abaixo, Adicien uma versão específica ou instale a mais recente:
```bash
sudo kubeadm init --kubernetes-version=v1.27.2
sudo kubeadm init
```

**Caso der algo errado, execute o comando abaixo para resetar:**
```bash
sudo kubeadm reset
```

Quando o processo terminal, será apresentado umas linhas parecidas com estas abaixo:
```bash
Your Kubernetes control-plane has initialized successfully!

To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

Alternatively, if you are the root user, you can run:

  export KUBECONFIG=/etc/kubernetes/admin.conf
```

Neste arquivo **admin.conf** contém todas as informações para o administrador do cluster e o próprio kubeadm. Sem estas informações é impossível se conectar e gerenciar o Kubernetes.
Faça este procedimento, pois é o ajuste para utilizar e administrar o cluster Kubernetes.

## Autocomplete

Adicionar o Auto Complete:
```bash
kubectl completion bash | sudo tee /etc/bash_completion.d/kubectl > /dev/null
```

## Plugin de rede

Se executarmos o comando `kubectl get nodes` podemos perceber que o **status** está **NotReady**. Executando também `kubectl get pod -n kubesystem`, veremos que dois Pods do **coredns** estão parados.
O que está faltando agora, é o plugin de redes para gerenciar nossa rede, distribuir IP's, Network Policy e outros.

Para buscar um plugin de rede, você pode ir até a **documentação do Kubernetes e digitar na busca addon**. existem vários, mas o utilizado será o **Weave Net**. Execute para instalar:
```bash
kubectl apply -f https://github.com/weaveworks/weave/releases/download/v2.8.1/weave-daemonset-k8s.yaml
```

Executando novamente a verificação com o comando `kubectl get nodes`, veremos que o **status** está **Ready**. Se executarmos `kubectl get pod -n kube-system`, veremos que dois Pods do **coredns** estão ativos e também foi criado um novo pod com o nove **weave-net-xxxxx**.


## Fonte:
- Container Runtimes - https://kubernetes.io/docs/setup/production-environment/container-runtimes/
- containerd - https://kubernetes.io/docs/setup/production-environment/container-runtimes/#containerd-systemd
- COnfigurando o containerd no config.toml - https://kubernetes.io/docs/setup/production-environment/container-runtimes/#containerd-systemd
- https://kubernetes.io/pt-br/docs/setup/production-environment/tools/kubeadm/install-kubeadm/
- Criando um cluster com kubeadm - https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/
- Plugin de rede - https://kubernetes.io/docs/concepts/cluster-administration/addons/


https://estacio.br/inscricao/formulario