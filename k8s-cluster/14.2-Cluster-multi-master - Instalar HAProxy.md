
## Instalar o HAProxy
```bash
sudo apt-get update && sudo apt-get install -y haproxy
```

## Configurar
```bash
vim /etc/haproxy/haproxy.cfg
```

Insira no final do arquivo:
```bash
frontend kubernetes
    bind 192.168.1.60:6443
    mode tcp
    option tcplog
    default_backend k8s-backend

backend k8s-backend
    mode tcp
    option tcp-check
    balance roundrobin
    server k8s-master1 192.168.1.61:6443 check check fall 3 rise 2
    server k8s-master2 192.168.1.62:6443 check check fall 3 rise 2
    server k8s-master3 192.168.1.63:6443 check check fall 3 rise 2
```

Reinicie o Serviço
```bash
sudo systemctl restart haproxy
```

Comandos úteis
```bash
tail -f /var/log/haproxy.log
nc -v 192.168.1.60 6443
```

## Fonte:
- https://www.kubesphere.io/docs/v3.3/installing-on-linux/high-availability-configurations/set-up-ha-cluster-using-keepalived-haproxy/