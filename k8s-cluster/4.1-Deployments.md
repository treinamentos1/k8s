Quando criamos um ReplicaSet, criamos um ou mais Pods. Um Deploy faz o mesmo que o ReplicaSet, mas adicionando melhorias.
O principal 

Podemos criar um deploy na linha de comando:
```bash
kubectl create deployment meudeploy --image=nginx --replicas=2
```
Para salvar o YAML criado por este comando, acrescente no final da linha `--dry-run=client -o yaml`

Para escalar UP ou Down a quantidade de Pods, executamos:
```bash
kubectl scale deployment meudeploy --replicas=3
```

Basicamente o que muda entre o ReplicaSet e Deployment é o **kind**
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: meu-deploy
spec:
  template:
    metadata:
      labels:
        app: deploy-nginx
    spec:
      containers:
      - name: container-deploy
        image: nginx 
  replicas: 2
  selector:
    matchLabels:
      app: deploy-nginx
```

Criando o deploy com o manifesto YAML
```bash
kubectl create -f deploy.yaml
```

Verificando o deploy:
```bash
kubectl get deploy
kubectl get rs
kubectl get pod
kubectl get all
```