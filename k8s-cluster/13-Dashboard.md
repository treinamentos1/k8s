```bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.7.0/aio/deploy/recommended.yaml
```

Este manifesto cria uma dashboard e vários objetos dentro dela:
```bash
kubectl get all -n kubernetes-dashboard
```

Vamos editar o Service **service/kubernetes-dashboard** com o parâmtro **type: NodePort**
```bash
kubectl edit -n kubernetes-dashboard service/kubernetes-dashboard
```

Liste e confirme que ganharemos uma porta de acesso :
```bash
kubectl get all -n kubernetes-dashboard
```

Acesse no Browser: **https://IP:PORTA** mas não ser frustre, ainda precisamos criar um token para acesso.


## Criar Service Account
Vamos criar uma conta de serviço para acessar a dashboard criada anteriormente:
`vim serviceaccount.yaml`
```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: admin-user
  namespace: kubernetes-dashboard
```

Crie e liste este serviceaccount criado:
```bash
kubectl create -f serviceaccount.yaml
kubectl get serviceaccounts -n kubernetes-dashboard
```

## Criar a RoleBinding
`vim rolebinding.yaml`
```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: admin-user
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: admin-user
  namespace: kubernetes-dashboard
```
Crie e liste o ClusterRoleBinding
```bash
kubectl create -f rolebinding.yaml
kubectl get clusterrolebindings -n kubernetes-dashboard
```

Obtendo o token e salve em um arquivo
```yaml
echo "kubectl -n kubernetes-dashboard create token admin-user | tee" > /bin/token-dashboard && chmod +x /bin/token-dashboard
```

Execute o script, insira na Dashboard e conseguirá o acesso.

## Fonte:
- https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/#deploying-containerized-applications
- https://github.com/kubernetes/dashboard/blob/master/docs/user/access-control/creating-sample-user.md
