## Desativar Swap
Parar o Swap do sistema:
```bash
sudo swapoff -a
```

Vá até o arquivo abaixo e desative qualquer menção a Swap
`sudo vim /etc/fstab`
```config
#/swap.img      none    swap    sw      0       0
```

## Container Runtimes

Encaminhando o IPv4 e permitindo que o iptables veja o tráfego em ponte:
```bash
sudo cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
overlay
br_netfilter
EOF

sudo modprobe overlay && \
sudo modprobe br_netfilter

sudo cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-iptables  = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward                 = 1
EOF

sudo sysctl --system
```

O principal a ser instalado neste momento é apenas o **containerd.io**, mas vamos seguindo com a instalação de todos os componentes da documentação do Docker.

```bash
sudo apt-get update && \
sudo apt-get install ca-certificates curl gnupg && \
sudo install -m 0755 -d /etc/apt/keyrings && \
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg && \
sudo chmod a+r /etc/apt/keyrings/docker.gpg && \
echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null && \
sudo apt-get update && \
sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin && \
sudo usermod -aG docker $USER && \
sudo init 6
```

## cni-plugins - Instalando um Pod de complemento de rede

Você deve implantar um Interface de rede do contêiner Complemento de rede de pods baseado em CNI para que seus pods possam se comunicar uns com os outros. O Cluster DNS (CoreDNS) não será inicializado antes da instalação de uma rede. 

Procure na documentação por **Container Runtimes** e vá até a página do GitHub ne etapa 3, ou siga estas instruções abaixo.
Troque a versão v1.3.0 pela versão mais recente ou da sua escolha, caso precisar.
```shell
wget https://github.com/containernetworking/plugins/releases/download/v1.3.0/cni-plugins-linux-amd64-v1.3.0.tgz && \
sudo mkdir -p /opt/cni/bin && \
sudo tar Cxzvf /opt/cni/bin cni-plugins-linux-amd64-v1.3.0.tgz && \
rm -rf cni-plugins-linux-amd64-v1.3.0.tgz
```

## Configurar o Containerd no arquivo config.toml
Para usar o driver do container runtime, adicione esta linha no final do arquivo:

`sudo vim /etc/containerd/config.toml`
```bash
[plugins."io.containerd.grpc.v1.cri".containerd.runtimes.runc.options]
    SystemdCgroup = true
```

Encontre e desabilite(comente) a linha abaixo:
```config
#disabled_plugins = ["cri"]
```

Reinicie o containerd
```bash
sudo systemctl restart containerd
```

## Instalando o kubeadm

- kubeadm: o comando para criar o cluster.
- kubelet: o componente que executa em todas as máquinas no seu cluster e cuida de tarefas como a inicialização de pods e contêineres.
- kubectl: a ferramenta de linha de comando para interação com o cluster.

Atualize o índice de pacotes apt e instale os pacotes necessários para utilizar o repositório apt do Kubernetes:
```bash
sudo apt-get update && \
sudo apt-get install -y apt-transport-https ca-certificates curl
```

Faça o download da chave de assinatura pública da Google Cloud:
```bash
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
```
Adicione o repositório apt do Kubernetes:
```bash
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list && \
sudo apt-get update
```

Atualize o índice de pacotes apt, instale o kubelet, o kubeadm e o kubectl, e fixe suas versões. Caso desejar, você pode ver a versão específica para ser instalada:
```bash
apt list -a kubeadm

sudo apt-get install -y kubeadm && \
sudo apt-mark hold kubeadm
```

## Solicitando e inserindo token

Ajuste e hostname do haproxy
`sudo vim /etc/hosts`
```bash
192.168.1.60    haproxy
```

Execute o comando abaixo no **NODE MASTER** necessária para se associar um worker ao cluster usando o token.
```bash
kubeadm token create --print-join-command
```

Execute o token como **root** no **NODE WORKER**
```bash
kubeadm join 192.168.1.50:6443 --token c8c1mu.dtprq1cygsejx5rj --discovery-token-ca-cert-hash sha256:6f8cff2bc1705bc77d79c6756e80ec1e7cc8641c41f414c017dbb4ee7d84e962
```

Vá até o Master e execute para verificar os nodes do Cluster
```bash
kubectl get nodes
```



