## Criando um pod com EmptyDir (Diretório vazio)
Um volume emptyDir é criado pela primeira vez quando um Pod é criado em um nó e existe enquanto esse Pod estiver sendo executado nesse nó. Como o nome diz, o volume emptyDir está inicialmente vazio. Todos os contêineres no Pod podem ler e gravar os mesmos arquivos no volume emptyDir, embora esse volume possa ser montado no mesmo caminho ou em caminhos diferentes em cada contêiner. Quando um Pod é removido de um nó por qualquer motivo, os dados no emptyDir são eliminados permanentemente.

Nota: A falha de um contêiner não remove um Pod de um nó. Os dados em um volume emptyDir são mantidos em caso de falha do contêiner.

Alguns usos para um emptyDir são:
- armazenamento temporário
- Coleta de logs
- manter arquivos que um contêiner gerenciador de conteúdo busca enquanto um contêiner de webserver entrega os dados

`vim emptidy.yaml`
```yaml
apiVersion: v1
kind: Pod
metadata:
  name: test-pd
spec:
  containers:
  - image: nginx
    name: test-container
    volumeMounts:
    - mountPath: /cache
      name: cache-volume
  volumes:
  - name: cache-volume
    emptyDir: {}
```

Podemos visualizar a montagem no describe do Pod, na sessão **test-container: -> Mounts:**
```bash
kubectl get pod test-pd
```

Acesse o Pod e crie um arquivo:
```bash
kubectl exec -it pods/test-pd -- bash
cd /cache/
touch teste.txt
```

**ACESSE NODE WORKER como root** onde o pod foi criado e filtre nos diretórios de volumes de Pods, onde este volume foi criado:
```bash
find /var/lib/kubelet/pods -name "cache-volume"
```

## Deletando Pod

Vamos deletar o Pod e ver o que acontece...
```bash
kubectl delete pod test-pd
```

**ACESSE NODE WORKER como root** onde o pod foi criado e pode confirmar que o volume foi removido e não existe mais.

## Fonte:
- https://kubernetes.io/docs/concepts/storage/volumes/#emptydir