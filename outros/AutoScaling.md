# Autoscaling

## Preparando o ambiente
Iremos executar o tutorial oficial para autoscaling.

https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale-walkthrough/#before-you-begin


#### Criar o Pod php-Apache
Para isso iremos rodar e expor o php-apache server

Caso esteja utilizando no Lab, desabilitar o monitoramento com prometheus e Grafana para o Autoscaling poder funcionar.

O arquivo _php-apache.yaml_ contém um deployment que limita o Pod para o mínimo de 20% e máximo de 50% de um núcleo de processador. Com este limite iremos estressar o Pod mais à frente.
```bash
kubectl apply -f arquivos/php-apache.yaml
```

#### Criando o AutoScaling

Agora iremos fazer a criação do Pod Autoscaler
```bash
kubectl apply -f arquivos/hpa.yaml
```

Para criar manualmente pode ser feito com o seguinte comando:
```bash
kubectl autoscale deployment php-apache --cpu-percent=50 --min=1 --max=5
```

Para listar o horizontal pod autoscale(HPA), Execute
```bash
kubectl get hpa
```

### Aumentar a carga

Agora iremos criar um pod chamado _load-generator_ para aumentar a carga no pod do apache em php.
```bash
kubectl run -it load-generator --image=alpine /bin/sh

while true; do wget -q -O- http://php-apache.default.svc.cluster.local; done
```

Agora iremos em outro terminal, com o kubectl, verificar como está o HPA, e também no painel do Rancher.

$ kubectl get hpa
$ kubectl get deployment php-apache
