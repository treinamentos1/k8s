### Comandos básicos e recursos:

- **Comandos**
  - create
  - delete
  - describe
  - get
  - apply
  - version

- **recursos**
  - configmaps
  - namespaces
  - nodes
  - pods
  - secrets
  - services
  - deployments
  - replicasets
  - ingresses
