Nesse exercício do liveness, iremos testar como fazer para dizer ao kubernetes, quando recuperar a nossa aplicação, caso alguma coisa aconteça a ela.
```js
http.HandleFunc("/healthz", func(w http.ResponseWriter, r *http.Request) { 
	duration := time.Now().Sub(started) 
	if duration.Seconds() > 10 { 
		w.WriteHeader(500) 
		w.Write([]byte(fmt.Sprintf("error: %v", duration.Seconds()))) 
	} else { 
		w.WriteHeader(200) 
		w.Write([]byte("ok")) 
	} 
})
```

O código acima, está dentro do container que iremos rodar. Nesse código, perceba que tem um IF, que irá fazer que de vez em quando a aplicação responda dando erro.

Como a aplicação irá retornar um erro, o serviço de liveness que iremos usar no Kubernetes, ficará verificando se a nossa aplicação está bem, e como ela irá falhar de tempos em tempos, o kubernetes irá reiniciar o nosso serviço.

`vim liveness.yaml`

```yaml
---
apiVersion: v1
kind: Pod
metadata:
  labels:
    test: liveness
  name: liveness-http
  # namespace: <nome-aluno>
spec:
  containers:
  - name: liveness
    image: k8s.gcr.io/liveness
    args:
    - /server
    livenessProbe:
      httpGet: # Faz um http get
        path: /healthz # neste contexto
        port: 8080 # nesta porta
        httpHeaders: # faz esta insercao no header para a validacao do servico
        - name: X-Custom-Header 
          value: Awesome
      initialDelaySeconds: 6 # inicialmente espera 6 segundos para a checagem
      periodSeconds: 3 # o periodo de execucao vai ser a cada 3 segundos
```

Depois de 10 segundos, verificamos que o container reiniciou. 
```bash
kubectl apply -f liveness.yml 
kubectl describe pod liveness-http 
kubectl get pod liveness-http 
```
