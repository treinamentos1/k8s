etc é um banco de dados

## Backup
Instale o Client do etcd:
```bash
sudo apt install -y etcd-client
```

Vamos criar um único container para testar a restauração do Backup.
```bash
kubectl run app-etcd --image=nginx --port=80
```

Colete as informações necessárias para o Backup no Pod etc-master na namespace kube-system:
**Substitua o nome do container etcd-master, caso necessário**
```bash
kubectl describe -n kube-system pod etcd-master
```
Colete as informações:
- --cert-file=/etc/kubernetes/pki/etcd/server.crt
- --key-file=/etc/kubernetes/pki/etcd/server.key
- --trusted-ca-file=/etc/kubernetes/pki/etcd/ca.crt

**Eleve o usuário para root.**

Baseado na documentação oficial, vamos montar a linha de comando para o Backup:
```bash
ETCDCTL_API=3 etcdctl --endpoints=https://192.168.1.50:2379 \
  --cacert=/etc/kubernetes/pki/etcd/ca.crt \
  --cert=/etc/kubernetes/pki/etcd/server.crt \
  --key=/etc/kubernetes/pki/etcd/server.key \
  snapshot save /opt/backup-etcd.db
```

## Restore

Vamos remover o Pod de teste.
```bash
kubectl delete pod app-etcd
kubectl get pods
```

O etcd é um Pod estático, sendo executado no Node **master**, vamos até o YAML coletar a informação 
```bash
sudo cat /etc/kubernetes/manifests/etcd.yaml
```

Caso você não tenha personalizado a instalação, o caminho será este:
```yaml
  - hostPath:
      path: /var/lib/etcd
```

**Eleve o usuário para root.**

Tendo este caminho, vamos descarregar o backup em um caminho praticamente igual. O motivo é obvio, o etcd está em execução persistindo os dados neste caminho.
```bash
ETCDCTL_API=3 etcdctl snapshot restore /opt/backup-etcd.db --data-dir=/var/lib/etcd-backup
```

Agora, vamos editar o manifesto YAML, adicionando o novo caminho de restore:
```yaml
  - hostPath:
      path: /var/lib/etcd
```

Neste instante, a instância do etcd será morta e será recriada, vai demorar alguns instantes para tudo ser restaurado.
Após restaurar, faça a verificação dos pods novamente e o pod restaurado estará lá.
```bash
kubectl get pods
```

## Fonte
- https://kubernetes.io/docs/tasks/administer-cluster/configure-upgrade-etcd/#built-in-snapshot