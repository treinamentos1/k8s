Para visualizar as informações dos recurrsos sendo consumidos pelos nodes e pods, podemos implantar o Pod de Métricas.
Vamos criar o Pod
```bash
kubectl apply -f https://raw.githubusercontent.com/pythianarora/total-practice/master/sample-kubernetes-code/metrics-server.yaml
```

Quando executadmos o comando acima, criamos um Pod dentro da Namespace _kube-system_. Para visualizar se está ou não ativo, podemoe executar:
```bash
kubectl get pods -n kube-system
```

Após o Pod subir, podemos ver as métricas das seguintes maneiras:
```bash
kubectl top node
kubectl top pod -n kube-system
kubectl top pod --all-namespace
```