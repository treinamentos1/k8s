ServiceAccount + Role

## Criando ServiceAccount e Role

Criando uma Service Account
```bash
kubectl create serviceaccount conta1 
```

Criando uma Role
```bash
kubectl create role conta1 --verb=get,list,watch --resource=pods
```

Vinculando a Role em uma ServiceAccount
```bash
kubectl create rolebinding conta1 --role=conta1 --serviceaccount=default:conta1
```

Listando
```bash
kubectl get sa,roles,rolebinding
```

Verificando as permissões deste usuário
```bash
kubectl auth can-i get pods --as=system:serviceaccount:default:conta1 -n default
kubectl auth can-i create pods --as=system:serviceaccount:default:conta1 -n default
```

Para editar a Role
```bash
kubectl edit role conta1
```

