CronJob destina-se a executar ações agendadas regulares, como backups, geração de relatórios, e assim por diante.
Um objeto CronJob é como uma linha de um arquivo crontab
```bash
# ┌───────────── minute (0 - 59)
# │ ┌───────────── hour (0 - 23)
# │ │ ┌───────────── day of the month (1 - 31)
# │ │ │ ┌───────────── month (1 - 12)
# │ │ │ │ ┌───────────── day of the week (0 - 6) (Sunday to Saturday;
# │ │ │ │ │                                   7 is also Sunday on some systems)
# │ │ │ │ │                                   OR sun, mon, tue, wed, thu, fri, sat
# │ │ │ │ │
# * * * * *
```

Um Job cria um ou mais Pods. À medida que os pods são concluídos com êxito, o Trabalho rastreia as conclusões bem-sucedidas.

### Exemplo de Job
```bash
kubectl create job meujob --image alpine -- date
kubectl logs -f jobs/meujob
kubectl delete jobs/meujob
```

Exemplo de cronjob
```bash
kubectl create cronjob meucronjob --image=alpine --schedule="*/1 * * * *" -- date
kubectl logs -f jobs/meucronjob-******
kubectl delete cronjobs/meucronjob
```

## Fontes
- https://kubernetes.io/docs/concepts/workloads/controllers/job/
- https://kubernetes.io/docs/concepts/workloads/controllers/cron-jobs/